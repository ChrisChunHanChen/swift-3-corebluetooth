//
//  BluetoothTableViewController.swift
//  CoreBluetooth Test
//
//  Created by Chris Chen on 2019/1/4.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit
import CoreBluetooth

class BluetoothTableViewController: UITableViewController {

    let deviceCell = "BluetoothTableViewCell"
    var centeralManager: CBCentralManager?
    
    // 儲存掃瞄到的 peripheral 物件
    var BTPeripheral:[CBPeripheral] = []
    // 儲存各個藍芽裝置是否可連線
    var BTIsConnectable: [Int] = []
    // 儲存各個藍芽裝置的訊號強度
    var BTRSSI:[NSNumber] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title = "Bluetooth Devices"
        navigationItem.title = title
        
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh,
                                            target: self, action: #selector(startScan))
        navigationItem.rightBarButtonItem = refreshButton
        
        // Register nibs for table view cells
        tableView.register(BluetoothTableViewCell.self,
                           forCellReuseIdentifier: deviceCell)
        
        // Setup for table view automatic row height calculation
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension

        centeralManager = CBCentralManager()
        centeralManager?.delegate = self
    }
    
    
    @objc func startScan() {
        navigationItem.rightBarButtonItem?.isEnabled = false
        BTPeripheral.removeAll(keepingCapacity: false)
        BTRSSI.removeAll(keepingCapacity: false)
        BTIsConnectable.removeAll(keepingCapacity: false)
        centeralManager!.scanForPeripherals(withServices: nil, options: nil)
        
        Timer.scheduledTimer(timeInterval: 5,
                             target: self,
                             selector: #selector(stopScan),
                             userInfo: nil,
                             repeats: false)
    }
    
    @objc func stopScan() {
        tableView.reloadData()
        centeralManager!.stopScan()
        navigationItem.rightBarButtonItem?.isEnabled = true

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BTPeripheral.count
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: deviceCell,
                                                    for: indexPath) as? BluetoothTableViewCell {

            cell.nameDetailsLabel.text = BTPeripheral[indexPath.row].name
            cell.idDetailsLabel.text = BTPeripheral[indexPath.row].identifier.uuidString
            cell.rssiDetailsLabel.text = "\(BTRSSI[indexPath.row])"

            let distancePower = Double(abs(BTRSSI[indexPath.row].intValue) - 70) / Double(10 * 1)
            cell.distanceDetailsLabel.text = "\(pow(10.0,distancePower)) M"
            cell.connectDetailsLabel.text = BTIsConnectable[indexPath.row].description
            return cell
        }
        return UITableViewCell()
    }
}

extension BluetoothTableViewController: CBCentralManagerDelegate {
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let temp = BTPeripheral.filter { (tempPeripheral) -> Bool in
            return tempPeripheral.name == peripheral.name
        }
        
        if temp.count == 0 {
            BTPeripheral.append(peripheral)
            BTRSSI.append(RSSI)
            BTIsConnectable.append(Int((advertisementData[CBAdvertisementDataIsConnectable]! as AnyObject).description)!)
        }
        tableView.reloadData()
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("BT ON")
        case .poweredOff:
            print("BT OFF")
        case .resetting:
            print("BT RESSTING")
        case .unknown:
            print("BT UNKNOW")
        case .unauthorized:
            print("BT UNAUTHORIZED")
        case .unsupported:
            print("BT UNSUPPORTED")
        }
    }
}
