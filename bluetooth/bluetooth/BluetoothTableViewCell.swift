    //
//  BluetoothTableViewCell.swift
//  CoreBluetooth Test
//
//  Created by Chris Chen on 2019/1/4.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit
import SnapKit

class BluetoothTableViewCell: UITableViewCell {

    var contentSubview = UIView()
    
    let nameLabel = UILabel()
    let nameDetailsLabel = UILabel()
    let rssiLabel = UILabel()
    let rssiDetailsLabel = UILabel()
    let distanceLabel = UILabel()
    let distanceDetailsLabel = UILabel()
    let idLabel = UILabel()
    let idDetailsLabel = UILabel()
    let connectLabel = UILabel()
    let connectDetailsLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // ContentSubview
        contentSubview.backgroundColor = .white
        addSubview(contentSubview)
        
        contentSubview.snp.makeConstraints { (make) in
            make.top.equalTo(snp.top).offset(8)
            make.bottom.equalTo(snp.bottom).offset(-8)
            make.leading.equalTo(snp.leading).offset(8)
            make.trailing.equalTo(snp.trailing).offset(-8)
        }
        
        addContentSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Helper functions
    
    func addContentSubviews() {
        
        // nameLabel
        nameLabel.text = "Name:"
        contentSubview.addSubview(nameLabel)
        
        // nameDetailsLabel
        nameDetailsLabel.text = " "
        contentSubview.addSubview(nameDetailsLabel)
        
        // rssiLabel
        rssiLabel.text = "RSSI:"
        contentSubview.addSubview(rssiLabel)
        
        // rssiDetailsLabel
        rssiDetailsLabel.text = " "
        contentSubview.addSubview(rssiDetailsLabel)
        
        // distanceLabel
        distanceLabel.text = "Distance:"
        contentSubview.addSubview(distanceLabel)
        
        // distanceDetailsLabel
        distanceDetailsLabel.text = " "
        contentSubview.addSubview(distanceDetailsLabel)
        
        // idLabel
        idLabel.text = "UUID:"
        contentSubview.addSubview(idLabel)
        
        // idDetailsLabel
        idDetailsLabel.text = " "
        contentSubview.addSubview(idDetailsLabel)
        
        // connectLabel
        connectLabel.text = "Status:"
        contentSubview.addSubview(connectLabel)
        
        // connectDetailsLabel
        connectDetailsLabel.text = " "
        contentSubview.addSubview(connectDetailsLabel)
        
        addContentSubviewConstraints()
    }
    
    func addContentSubviewConstraints() {
        
        // nameLabel
        nameLabel.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.top.equalTo(contentSubview.snp.top).offset(8)
            make.leading.equalTo(contentSubview.snp.leading).offset(10)
        }
        
        // nameDetailsLabel
        nameDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.top)
            make.leading.equalTo(nameLabel.snp.trailing).offset(15)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-15)
        }
        
        // rssiLabel
        rssiLabel.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.top.equalTo(nameLabel.snp.bottom).offset(8)
            make.leading.equalTo(contentSubview.snp.leading).offset(10)
        }
        
        // rssiDetailsLabel
        rssiDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(rssiLabel.snp.top)
            make.leading.equalTo(rssiLabel.snp.trailing).offset(15)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-15)
        }
        
        // distanceLabel
        distanceLabel.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.top.equalTo(rssiLabel.snp.bottom).offset(8)
            make.leading.equalTo(contentSubview.snp.leading).offset(10)
        }
        
        // distanceDetailsLabel
        distanceDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(distanceLabel.snp.top)
            make.leading.equalTo(distanceLabel.snp.trailing).offset(15)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-15)
        }
        
        // idLabel
        idLabel.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.top.equalTo(distanceLabel.snp.bottom).offset(8)
            make.leading.equalTo(contentSubview.snp.leading).offset(10)
        }
        
        // idDetailsLabel
        idDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(idLabel.snp.top)
            make.leading.equalTo(idLabel.snp.trailing).offset(15)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-15)
        }
        
        // connectLabel
        connectLabel.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.top.equalTo(idLabel.snp.bottom).offset(8)
            make.bottom.equalTo(contentSubview.snp.bottom).offset(-10)
            make.leading.equalTo(contentSubview.snp.leading).offset(10)
        }
        
        // connectDetailsLabel
        connectDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(connectLabel.snp.top)
            make.leading.equalTo(connectLabel.snp.trailing).offset(15)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-15)
        }
    }
}
